const express = require('express');
const path = require("path");
const app = express();
const fs = require("fs");
const morgan = require('morgan');

const EXT_ALLOWED = ['.js', '.html', '.xml', '.json', '.yaml', '.log', '.txt'];
const BAD_FILE_NAME = " ";



app.use(express.json());
app.use(morgan('tiny'));

fs.stat(path.join(__dirname, './all_files'), (e) => {
    if (e) {
        fs.mkdir('all_files', () => {});
    }
})


app.post('/api/files', (req, res) => {
    console.log(`${req.method} ${req.url}`, req.body);   

    const extention = path.extname(req.body.filename);

    if(!EXT_ALLOWED.includes(extention)){
        return res.status(400).json({message: "Wrong extention of the file!"}); 
    }
    
     if(!req.body.content){
        return res.status(400).json({message: "Please specify 'content' parameter"});
    } else if (!req.body.filename || req.body.filename.includes(BAD_FILE_NAME)){
        return res.status(400).json({message: "Please specify correct 'filename' parameter"});
    }

    if (req.body.password) {

        fs.mkdir('./all_files', () => {});
        let reqBody = JSON.stringify(req.body);
        fs.writeFile(path.join(__dirname, './all_files', req.body.filename), reqBody, function(e) {
            if (e){
                return res.status(500).json({message: "Server error"});
            } else {
               return res.status(200).json({message:"File created successfully"});
            }
        });
    } else {
        fs.writeFile(path.join(__dirname, './all_files', req.body.filename), req.body.content, function(e) {
            if (e){
                return res.status(500).json({message: "Server error"});
            } else {
                return res.status(200).json({message:"File created successfully"})
            }
        });
    }
})
    


app.get('/api/files', (req, res) => {
    console.log(`${req.method} ${req.url}`, req.body);  

    fs.readdir('./all_files', (e, files) => {
        if(e) {
            return res.status(500).json({message: "Server error"});
        }

        if (!files.length) {
            return res.status(400).json({ message: "Client error"});
          }
      
        return res.status(200).json({"message": "Success",
            "files": files});
        })   
})

app.get('/api/files/:filename', function (req, res) {
    console.log(`${req.method} ${req.url}`, req.params);
    console.log(req.query.password)
        

    if (req.query.password) {
        let {password} = req.query;
        let reqParams = req.params;
    
        fs.readFile(path.join(__dirname, './all_files', reqParams.filename), 'utf8', function (e, file) {
            if (e) {
                return res.status(400).json({message: "No file with" + " " + reqParams.filename + " " + "filename found"});
            }
            
            let obj = JSON.parse(file);
            let truePass = obj.password;
            if(password === truePass){
                let { birthtime } = fs.statSync(path.join(__dirname, './all_files', reqParams.filename))
                return res.status(200).json({
                    "message": "Success",
                    "filename": reqParams.filename,
                    "content": file,
                    "extension": path.extname(reqParams.filename),
                    "uploadedDate": birthtime  
                })
            } else {
                return res.status(400).json({message: "Wrong password!"})
            }
        });
    } else {
    let paramsName = req.params;

    fs.readFile(path.join(__dirname, './all_files', paramsName.filename), 'utf8', function (e, file) {
        if (e) {
            return res.status(400).json({message: "No file with" + " " + paramsName.filename + " " + "filename found"});
        } 
      

        if(file.includes('password')){
            return res.status(400).json({message: "Please specify the password"});
        }
        
        let { birthtime } = fs.statSync(path.join(__dirname, './all_files', paramsName.filename))
        return res.status(200).json({
            "message": "Success",
            "filename": paramsName.filename,
            "content": file,
            "extension": path.extname(paramsName.filename),
            "uploadedDate": birthtime  
          })
        
      });
    }
})

  app.delete('/api/files/:filename', function (req, res) {
    console.log(`${req.method} ${req.url}`, req.params);

    let info = req.params;

    fs.unlink(path.join(__dirname, './all_files', info.filename), (err) => {
        if (err) {
            return res.status(400).json({message: "No file with" + " " + info.filename + " " + "filename found"});
        } else {
            return  res.status(200).json({"message": "The file was successfully deleted"})
        }
       
    });

  });  

  app.put('/api/files/:filename', function (req, res) {
    console.log(`${req.method} ${req.url}`, req.params);

    let infoAboutReq = req.params;

    fs.readFile(path.join(__dirname, './all_files', infoAboutReq.filename), 'utf8', function (e, file) {
        if (e) {
            return res.status(400).json({message: "No file with" + " " + infoAboutReq.filename + " " + "filename found"});
        } 
        const newFileContent = file.replace(file, req.body.content);
        fs.writeFile(path.join(__dirname, './all_files', infoAboutReq.filename), newFileContent, 'utf8', function (e) {
            if (e) return res.status(500).json({message: "Server error"});
            return res.status(200).json({message: "The file was successfully updated"})
         });
    
    })
  });

  app.all('*', function (req, res, next) {
    res.status(404).send('Not found');
  });

app.listen(8080, ()=>{
    console.log('Server works')
})

